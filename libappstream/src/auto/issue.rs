// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use crate::{ffi, IssueKind};
use glib::{prelude::*, translate::*};

glib::wrapper! {
    #[doc(alias = "AsIssue")]
    pub struct Issue(Object<ffi::AsIssue, ffi::AsIssueClass>);

    match fn {
        type_ => || ffi::as_issue_get_type(),
    }
}

impl Issue {
    pub const NONE: Option<&'static Issue> = None;

    #[doc(alias = "as_issue_new")]
    pub fn new() -> Issue {
        assert_initialized_main_thread!();
        unsafe { from_glib_full(ffi::as_issue_new()) }
    }
}

impl Default for Issue {
    fn default() -> Self {
        Self::new()
    }
}

mod sealed {
    pub trait Sealed {}
    impl<T: super::IsA<super::Issue>> Sealed for T {}
}

pub trait IssueExt: IsA<Issue> + sealed::Sealed + 'static {
    #[doc(alias = "as_issue_get_id")]
    #[doc(alias = "get_id")]
    fn id(&self) -> Option<glib::GString> {
        unsafe { from_glib_none(ffi::as_issue_get_id(self.as_ref().to_glib_none().0)) }
    }

    #[doc(alias = "as_issue_get_kind")]
    #[doc(alias = "get_kind")]
    fn kind(&self) -> IssueKind {
        unsafe { from_glib(ffi::as_issue_get_kind(self.as_ref().to_glib_none().0)) }
    }

    #[doc(alias = "as_issue_get_url")]
    #[doc(alias = "get_url")]
    fn url(&self) -> Option<glib::GString> {
        unsafe { from_glib_none(ffi::as_issue_get_url(self.as_ref().to_glib_none().0)) }
    }

    #[doc(alias = "as_issue_set_id")]
    fn set_id(&self, id: &str) {
        unsafe {
            ffi::as_issue_set_id(self.as_ref().to_glib_none().0, id.to_glib_none().0);
        }
    }

    #[doc(alias = "as_issue_set_kind")]
    fn set_kind(&self, kind: IssueKind) {
        unsafe {
            ffi::as_issue_set_kind(self.as_ref().to_glib_none().0, kind.into_glib());
        }
    }

    #[doc(alias = "as_issue_set_url")]
    fn set_url(&self, url: &str) {
        unsafe {
            ffi::as_issue_set_url(self.as_ref().to_glib_none().0, url.to_glib_none().0);
        }
    }
}

impl<O: IsA<Issue>> IssueExt for O {}
