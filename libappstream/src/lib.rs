#![allow(clippy::needless_doctest_main)]
#![cfg_attr(docsrs, feature(doc_cfg))]
#![doc(
    html_logo_url = "https://raw.githubusercontent.com/ximion/appstream/master/docs/images/src/svg/appstream-logo.svg"
)]
//! # Rust AppStrean bindings
//!
//! This library contains safe Rust bindings for [AppStream](https://github.com/ximion/appstream), a library to work
//! with AppStream metadata.
//!
//! See also
//!
//! - [AppStream documentation](https://www.freedesktop.org/software/appstream/docs/)
//! - [AppStream on Freedesktop](https://www.freedesktop.org/wiki/Distributions/AppStream/)

/// No-op.
macro_rules! skip_assert_initialized {
    () => {};
}

// Re-export the -sys bindings
pub use ffi;
pub use gio;
pub use glib;

/// No-op.
macro_rules! assert_initialized_main_thread {
    () => {};
}

pub use auto::*;
pub use flags::*;

mod auto;
mod flags;

pub mod prelude;

pub mod functions {
    pub use super::auto::functions::*;
}
pub use functions::*;
