use glib::bitflags::bitflags;
use glib::translate::*;
use std::fmt;

bitflags! {
    /// The token match kind, which we want to be exactly 16 bits for storage
    /// reasons.
    #[derive(Debug)]
    #[doc(alias = "AsSearchTokenMatch")]
    pub struct SearchTokenMatch: u16 {
        /// No token matching
        #[doc(alias = "AS_SEARCH_TOKEN_MATCH_NONE")]
        const NONE = ffi::AS_SEARCH_TOKEN_MATCH_NONE as u16;
        /// Use the component mediatypes
        #[doc(alias = "AS_SEARCH_TOKEN_MATCH_MEDIATYPE")]
        const MEDIATYPE = ffi::AS_SEARCH_TOKEN_MATCH_MEDIATYPE as u16;
        /// Use the component package name
        #[doc(alias = "AS_SEARCH_TOKEN_MATCH_PKGNAME")]
        const PKGNAME = ffi::AS_SEARCH_TOKEN_MATCH_PKGNAME as u16;
        /// Use the app origin
        #[doc(alias = "AS_SEARCH_TOKEN_MATCH_ORIGIN")]
        const ORIGIN = ffi::AS_SEARCH_TOKEN_MATCH_ORIGIN as u16;
        /// Use the component description
        #[doc(alias = "AS_SEARCH_TOKEN_MATCH_DESCRIPTION")]
        const DESCRIPTION = ffi::AS_SEARCH_TOKEN_MATCH_DESCRIPTION as u16;
        #[doc(alias = "AS_SEARCH_TOKEN_MATCH_SUMMARY")]
        const SUMMARY = ffi::AS_SEARCH_TOKEN_MATCH_SUMMARY as u16;
        /// Use the component keyword
        #[doc(alias = "AS_SEARCH_TOKEN_MATCH_KEYWORD")]
        const KEYWORD = ffi::AS_SEARCH_TOKEN_MATCH_KEYWORD as u16;
        /// Use the component name
        #[doc(alias = "AS_SEARCH_TOKEN_MATCH_NAME")]
        const NAME = ffi::AS_SEARCH_TOKEN_MATCH_NAME as u16;
        /// Use the component ID
        #[doc(alias = "AS_SEARCH_TOKEN_MATCH_ID")]
        const ID = ffi::AS_SEARCH_TOKEN_MATCH_ID as u16;
    }
}

impl fmt::Display for SearchTokenMatch {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        <Self as fmt::Debug>::fmt(self, f)
    }
}

#[doc(hidden)]
impl IntoGlib for SearchTokenMatch {
    type GlibType = ffi::AsSearchTokenMatch;

    fn into_glib(self) -> ffi::AsSearchTokenMatch {
        self.bits()
    }
}

#[doc(hidden)]
impl FromGlib<ffi::AsSearchTokenMatch> for SearchTokenMatch {
    unsafe fn from_glib(value: ffi::AsSearchTokenMatch) -> Self {
        skip_assert_initialized!();
        Self::from_bits_truncate(value)
    }
}
