# libappstream-rs

[![Crates.io Version](https://img.shields.io/crates/v/libappstream?label=libappstream)
](https://crates.io/crates/libappstream-sys)
[![Crates.io Version](https://img.shields.io/crates/v/libappstream?label=libappstream-sys)
](https://crates.io/crates/libappstream-sys)

The Rust bindings of [appstream](https://github.com/ximion/appstream)

Website: <https://world.pages.gitlab.gnome.org/Rust/libappstream-rs/>

## Documentation

- appstream: <https://world.pages.gitlab.gnome.org/Rust/libappstream-rs/git/docs/libappstream>
- appstream-sys: <https://world.pages.gitlab.gnome.org/Rust/libappstream-rs/git/docs/libappstream_sys>
